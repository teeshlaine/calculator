import math

def main():
    operation = input("Enter operation (+, -, *, /, sin, cos, tan, log, sqrt): ")

    if operation in ["sin", "cos", "tan", "log", "sqrt"]:
        num = float(input("Enter number: "))
        if operation == "sin":
            print(f"sin({num}) = {math.sin(num)}")
        elif operation == "cos":
            print(f"cos({num}) = {math.cos(num)}")
        elif operation == "tan":
            print(f"tan({num}) = {math.tan(num)}")
        elif operation == "log":
            print(f"log({num}) = {math.log(num)}")
        elif operation == "sqrt":
            print(f"sqrt({num}) = {math.sqrt(num)}")
    else:
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        if operation == "+":
            print(f"{num1} + {num2} = {num1 + num2}")
        elif operation == "-":
            print(f"{num1} - {num2} = {num1 - num2}")
        elif operation == "*":
            print(f"{num1} * {num2} = {num1 * num2}")
        elif operation == "/":
            if num2 == 0:
                print("Error: Cannot divide by zero")
            else:
                print(f"{num1} / {num2} = {num1 / num2}")
    
    print(f"Value of pi: {math.pi}")

if __name__ == '__main__':
    main()
