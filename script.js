// Select all the buttons and the input field
const buttons = document.querySelectorAll('button');
const inputField = document.querySelector('input');

// Add event listeners to the buttons
buttons.forEach((button) => {
  button.addEventListener('click', (e) => {
    // Get the button's value
    const buttonValue = e.target.value;
    
    // If the button is "C", clear the input field
    if (buttonValue === 'C') {
      inputField.value = '';
    } 
    // If the button is "=", evaluate the expression in the input field
    else if (buttonValue === '=') {
      try {
        inputField.value = eval(inputField.value);
      } catch (error) {
        inputField.value = 'Error';
      }
    } 
    // Otherwise, append the button's value to the input field
    else {
      inputField.value += buttonValue;
    }
  });
});
